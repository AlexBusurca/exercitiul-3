package Ro.Orange;

public class Bedrooms {
    public static void main(String[] args) {
        SingleBeds singleBed = new SingleBeds("brown", "single bed",200,50,175);
        MatrimonialBeds matrimonialBed = new MatrimonialBeds("red","matrimonial bed", 200, 50, 455, "random", "Scandinavian Pine");
        singleBed.CalculateSize();
        System.out.println ("The color of Single Bed is: " +singleBed.getColor());
        matrimonialBed.CalculateSize();
        System.out.println ("Matrimonial Bed is made of " +matrimonialBed.getMadeOf());

    }
}
