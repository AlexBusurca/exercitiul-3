package Ro.Orange;

public class MatrimonialBeds extends SingleBeds {
    private String style;
    private String madeOf;

        public MatrimonialBeds (String setColor, String setType, int setDimension, int setHeight, int setWeight, String setStyle, String setMadeOf) {
            super(setColor, setType, setDimension, setHeight,setWeight);
            this.style = setStyle;
            this.madeOf = setMadeOf;
        }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStyle() {
        return style;
    }

    public void setMadeOf(String madeOf) {
        this.madeOf = madeOf;
    }

    public String getMadeOf() {
        return madeOf;
    }

    public String CalculateSize (){
        System.out.println(getDimension()+getHeight()+getWeight());
        return getColor();
    }

}
