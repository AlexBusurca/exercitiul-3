package Ro.Orange;

public class SingleBeds {
    private String color;
    private String type;
    private int dimension;
    private int height;
    private int weight;

    public SingleBeds(String setColor, String setType, int setDimension, int setHeight, int setWeight) {
        this.color = setColor;
        this.type = setType;
        this.dimension = setDimension;
        this.height = setHeight;
        this.weight = setWeight;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getDimension() {
        return dimension;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public String CalculateSize (){
        System.out.println(getDimension()+getHeight()+getWeight());
        return getColor();
    }
}
